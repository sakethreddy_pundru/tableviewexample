//
//  ViewController.m
//  Tableviewexample
//
//  Created by admin on 27/09/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "ViewController.h"

@interface UIViewController ()

@end

@implementation TableViewController{
NSArray *table;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    table = [[NSArray alloc]initWithObjects:@"News",@"Movies",@"Sports", nil];
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [table count];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [table count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [table objectAtIndex:indexPath.row];
    return cell;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
